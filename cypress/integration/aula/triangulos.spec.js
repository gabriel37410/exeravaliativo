import 'cypress-file-upload';
describe('Equilatero', function(){
    it('Equilatero', function(){
    cy.visit('localhost:4200')

    cy.get('#campo1')
      .click()
      .type('200')
    cy.get('#campo2')
      .click()
      .type('200')
    cy.get('#campo3')
      .click()
      .type('200')
      .type('{enter}')
    }) 
})

describe('Isoscele', function(){
    it('Isoscele', function(){
    cy.visit('localhost:4200')

    cy.get('#campo1')
    .click()
    .type('100')
    cy.get('#campo2')
    .click()
    .type('100')
    cy.get('#campo3')
    .click()
    .type('150')
    .type('{enter}')
    }) 
})

describe('Escaleno', function(){
    it('Escaleno', function(){
    cy.visit('localhost:4200')

    cy.get('#campo1')
    .click()
    .type('400')
    cy.get('#campo2')
    .click()
    .type('500')
    cy.get('#campo3')
    .click()
    .type('600')
    .type('{enter}')
    }) 
})